disp("Se�al Senoidal de 3Hz");
f = 3;
w = 2*pi*f;
t = 0:1/100:1;
fdt = sin(w*t);
plot(t,fdt,'lineWidth',3);